package map.park;

import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class ParkProjectActivity extends MapActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}

	public void parkNow(View v) {
		Parker pk = new Parker();
		pk.parkIt(getApplicationContext(), this);
		Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
				Toast.LENGTH_SHORT).show();
	}

	public void retrieveNow(View v) {
		Parker pk = new Parker();
		String locationInfo = "";

		try {
			pk.retrieveParking(getApplicationContext());
			locationInfo = pk.getlatitude() + " Lat, " + pk.getLongitude()
					+ " Long ";
		} catch (Exception e) {

			Log.e("Parker", e.getMessage(), e);
			locationInfo = "No last saved location on record";
		}
		showMap();
		Toast.makeText(getApplicationContext(), locationInfo, Toast.LENGTH_LONG)
				.show();

	}

	public void showMap() {
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		List<Overlay> mapOverlays = mapView.getOverlays();
		Drawable drawable = this.getResources().getDrawable(
				R.drawable.ic_launcher);
		HelloItemizedOverlay itemizedoverlay = new HelloItemizedOverlay(
				drawable, this);
		GeoPoint point = new GeoPoint(30443769, -91158458);
		OverlayItem overlayitem = new OverlayItem(point,
				"Laissez les bon temps rouler!", "I'm in Louisiana!");

		// GeoPoint point2 = new GeoPoint(17385812, 78480667);
		// OverlayItem overlayitem2 = new OverlayItem(point2, "Namashkaar!",
		// "I'm in Hyderabad, India!");

		itemizedoverlay.addOverlay(overlayitem);
		// itemizedoverlay.addOverlay(overlayitem2);

		mapOverlays.add(itemizedoverlay);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}