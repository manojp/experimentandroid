package map.park;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class Parker {
	
	private byte counter=0;
	private Location lastLocation = null;
	
	private LocationManager locationManager = null;
	private Context context = null;
	
	static final String FILENAME = "parkit_file";
	
	
	public void parkIt(Context ctx, Activity theActivity)
	{
		context = ctx;
		// Acquire a reference to the system Location Manager
		locationManager = (LocationManager) theActivity.getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.
		    	while(! makeUseOfNewLocation(location,this))
		    	{
		    		Log.d("Parker", "Location changed but waiting for more location update");
		    	}
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}
		  };

		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	}
	
	
	
	private boolean makeUseOfNewLocation(Location location, LocationListener listener){
		Log.d("Parker", "received new location : "+location);
		if(counter < 5){
			lastLocation = location;
			counter++;
			return false;
		}
		else{
			locationManager.removeUpdates(listener);
			try{
				saveLocation(location);
				return true;
			}catch(Exception ex){
				throw new RuntimeException("Could not store the parking location.",ex);
			}
		}
	}
	
	private void saveLocation(Location location) throws Exception
	{
		FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeLong(location.getTime());
		oos.writeDouble(location.getLatitude());
		oos.writeDouble(location.getLongitude());
		oos.writeDouble(location.getAltitude());
		oos.close();
		fos.close();
	}
	
	Date fix = null;
	double lati, longi, alti = 0;
	
	public void retrieveParking(Context ctx) throws Exception{
		FileInputStream fis = ctx.openFileInput(FILENAME);
		ObjectInputStream ois = new ObjectInputStream(fis);
		fix = new Date(ois.readLong());
		lati = ois.readDouble();
		longi = ois.readDouble();
		alti = ois.readDouble();
		ois.close();
		fis.close();
	}
	
	public double getlatitude(){
		return lati;
	}
	
	public double getLongitude(){
		return longi;
	}
	
	public double getAlti(){
		return alti;
	}
	
	

}
